package org.zeith.simplequarry.cfg;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.cfg.gui.HCConfigGui;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;
import org.zeith.simplequarry.InfoSQ;

import java.util.Collections;
import java.util.Set;

public class ConfigFactorySQ implements IModGuiFactory
{
	@Override
	public void initialize(Minecraft minecraftInstance)
	{
		HammerCore.LOG.info("Created Simple Quarry Gui Config Factory!");
	}
	
	@Override
	public boolean hasConfigGui()
	{
		return true;
	}
	
	@Override
	public GuiScreen createConfigGui(GuiScreen parentScreen)
	{
		return new HCConfigGui(parentScreen, ConfigsSQ.cfgs, InfoSQ.MOD_ID);
	}
	
	@Override
	public Set<IModGuiFactory.RuntimeOptionCategoryElement> runtimeGuiCategories()
	{
		return Collections.emptySet();
	}
}